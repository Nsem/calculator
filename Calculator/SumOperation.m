//
//  SumOperation.m
//  Calculator
//
//  Created by Nikita Semistrok on 01/11/2016.
//  Copyright © 2016 Nikita Semistrok. All rights reserved.
//

#import "SumOperation.h"

@implementation SumOperation

- (double)performWithSecondArgument:(double)secondArgument {
	return self.firstArgument + secondArgument;
}

@end
