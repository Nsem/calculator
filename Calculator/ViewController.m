//
//  ViewController.m
//  Calculator
//
//  Created by Nikita Semistrok on 01/11/2016.
//  Copyright © 2016 Nikita Semistrok. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	self.model = [[CalculatorModel alloc] init];
}

- (IBAction)pressDigit:(UIButton *)sender {
	if (self.isNumberInputting) {
		if (![sender.currentTitle isEqualToString:@"."] || ![self.numbericLabel.text containsString:@"."]) {
			self.numbericLabel.text = [self.numbericLabel.text stringByAppendingString:sender.currentTitle];
		}
	}
	else {
		if ([sender.currentTitle isEqualToString:@"."]) {
			self.numbericLabel.text = [NSString stringWithFormat:@"0."];
		}
		else {
			self.numbericLabel.text = sender.currentTitle;
		}
		self.numberInputting = YES;
	}
}

- (IBAction)performOperation:(UIButton *)sender {
	self.numberInputting = NO;
	double value = self.numbericLabel.text.doubleValue;
	NSString *operationString = sender.currentTitle;
	double result = [self.model performOperation:operationString withValue:value];
	self.numbericLabel.text = [@(result) stringValue];
}


@end
