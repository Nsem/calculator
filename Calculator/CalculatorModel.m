//
//  CalculatorModel.m
//  Calculator
//
//  Created by Nikita Semistrok on 01/11/2016.
//  Copyright © 2016 Nikita Semistrok. All rights reserved.
//

#import "CalculatorModel.h"
#import "SumOperation.h"
#import "SignOperation.h"
#import "EqualOperation.h"
#import "ACOperation.h"

#import "MinusOperation.h"
#import "MultOperation.h"
#import "DivOperation.h"
#import "PercentOperation.h"


@implementation CalculatorModel

- (instancetype)init {
	self = [super init];
	if (self) {
		_operations = @{
						@"AC" : [[ACOperation alloc] init],
						@"+" : [[SumOperation alloc] init],
						@"-" : [[MinusOperation alloc] init],
						@"±" : [[SignOperation alloc] init],
						@"=" : [[EqualOperation alloc] init],
						@"×" : [[MultOperation alloc] init],
						@"÷" : [[DivOperation alloc] init],
						@"%" : [[PercentOperation alloc] init],
						};
	}
	return self;
}

- (id)operationForOperationString:(NSString *)operationString {
	id operation = [self.operations objectForKey:operationString];
	if (!operation) {
		operation = [[UnaryOperation alloc] init];
	}
	return operation;
}

- (double)performOperation:(NSString *)operationString withValue:(double)value {
	id operation = [self operationForOperationString:operationString];
	
	if ([operation isKindOfClass:[EqualOperation class]]) {
		if (self.curentBinaryOperation) {
			value = [self.curentBinaryOperation performWithSecondArgument:value];
			return value;
		}
	}
	
	if ([operation isKindOfClass:[ACOperation class]]) {
		value = 0.0;
		self.curentBinaryOperation.firstArgument = value;
		[self.curentBinaryOperation performWithSecondArgument:value];
		return value;
	}
	
	if ([operation isKindOfClass:[UnaryOperation class]]) {
		return [operation perform:value];
	}
	
	if ([operation isKindOfClass:[BinaryOperation class]]) {
		if (self.curentBinaryOperation) {
			value = [self.curentBinaryOperation performWithSecondArgument:value];
		}
		self.curentBinaryOperation = operation;
		self.curentBinaryOperation.firstArgument = value;
		return value;
	}
	
	return DBL_MAX;
}

@end
