//
//  CalculatorModel.h
//  Calculator
//
//  Created by Nikita Semistrok on 01/11/2016.
//  Copyright © 2016 Nikita Semistrok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UnaryOperation.h"
#import "BinaryOperation.h"
#import "ACOperation.h"

@interface CalculatorModel : NSObject

@property (nonatomic, strong) NSDictionary *operations;
@property (nonatomic, strong) BinaryOperation *curentBinaryOperation;

- (double)performOperation:(NSString *)operationString withValue:(double)value;

@end
