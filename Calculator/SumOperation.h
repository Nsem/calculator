//
//  SumOperation.h
//  Calculator
//
//  Created by Nikita Semistrok on 01/11/2016.
//  Copyright © 2016 Nikita Semistrok. All rights reserved.
//

#import "BinaryOperation.h"

@interface SumOperation : BinaryOperation

@end
