//
//  SignOperation.m
//  Calculator
//
//  Created by Nikita Semistrok on 01/11/2016.
//  Copyright © 2016 Nikita Semistrok. All rights reserved.
//

#import "SignOperation.h"

@implementation SignOperation

- (double)perform:(double)value {
	return -value;
}

@end
