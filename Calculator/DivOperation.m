//
//  DivOperation.m
//  Calculator
//
//  Created by Nikita Semistrok on 16/11/2016.
//  Copyright © 2016 Nikita Semistrok. All rights reserved.
//

#import "DivOperation.h"

@implementation DivOperation

- (double)performWithSecondArgument:(double)secondArgument {
	return self.firstArgument / secondArgument;
}

@end
