//
//  UnaryOperation.m
//  Calculator
//
//  Created by Nikita Semistrok on 01/11/2016.
//  Copyright © 2016 Nikita Semistrok. All rights reserved.
//

#import "UnaryOperation.h"

@implementation UnaryOperation

- (double)perform:(double)value{
	return value;
}

@end
