//
//  MinusOperation.h
//  Calculator
//
//  Created by Nikita Semistrok on 16/11/2016.
//  Copyright © 2016 Nikita Semistrok. All rights reserved.
//

#import "BinaryOperation.h"

@interface MinusOperation : BinaryOperation

@end
