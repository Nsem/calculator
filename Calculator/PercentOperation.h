//
//  PercentOperation.h
//  Calculator
//
//  Created by Nikita Semistrok on 16/11/2016.
//  Copyright © 2016 Nikita Semistrok. All rights reserved.
//

#import "UnaryOperation.h"

@interface PercentOperation : UnaryOperation

@end
