//
//  ViewController.h
//  Calculator
//
//  Created by Nikita Semistrok on 01/11/2016.
//  Copyright © 2016 Nikita Semistrok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalculatorModel.h"

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *numbericLabel;
@property (nonatomic, assign, getter=isNumberInputting) BOOL numberInputting;
@property (nonatomic, strong) CalculatorModel *model;

- (IBAction)pressDigit:(UIButton *)sender;

- (IBAction)performOperation:(UIButton *)sender;

@end

