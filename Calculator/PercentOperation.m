//
//  PercentOperation.m
//  Calculator
//
//  Created by Nikita Semistrok on 16/11/2016.
//  Copyright © 2016 Nikita Semistrok. All rights reserved.
//

#import "PercentOperation.h"

@implementation PercentOperation

- (double)perform:(double)value {
	return value / 100;
}

@end
